# hello-world

git trial

 ## section 1

 一応テスト。
 追加。

 ## Gantt Chart Sample

 ```mermaid
gantt
    title Gantt Chart Test
    dateFormat YYYY-MM-DD
    axisFormat %m-%d
    excludes weekends
    
    section A
    task1(fixed date): a1, 2022-07-21, 2022-07-27
    task2(end date fixed, start days previous): a2, 10d, 2022-08-09

    section B
    math link: b2, after a2, 10d
    target1: milestone, b1, 2022-08-25, 0d
    click b2 href "test-math.md"
```
