test for markdown preview enhanced
===
<!--
<style>
    .no-page-break {
        page-break-inside: avoid;
    }
    .page-break {
        opacity: 0;
        break-after: page;
    }
</style>
-->
## Math

$`x^2`$

katexでレンダリングする数式

```math
y = x^2
```

`$`を使ったインライン数式はレンダリングされるのか？$y = \sum_{i = 1}^\infty x$

`$$`を使った別行立ての数式もきちんと表現できる。

$$
\begin{aligned}
y &= x \\
z &= \sin (x) + y
\end{aligned}
$$


## 画像の読込みと改ページ

このような画像があった場合に、HTMLをPDFにすると改ページによって画像が途切れることがあるかも知れない。


<div style="page-break-inside: avoid;">

![](16238333997620.jpg)

</div>

HTMLファイルのアップデートがかからないのは何故か？

右のプレビューでリロードをする必要があるのかも知れない。
